<?php

use App\Models\Venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pedido', function () {
    return view('formularios.NuevoPedido');
});

Route::put('buscar', function(Request $request){
    
    $codigo = $request->get('codigo');
        
    $libro = DB::table('stock')
    ->where('codigo','=',$codigo)
    ->get();
    
    return view('formularios.PedidoLibro')->with('libro',$libro);
});

Route::resource('venta','App\Http\Controllers\VentasController');

Route::get('venta/create/{codigo}', function($codigo){

    $libro = DB::table('stock')
    ->where('codigo','=',$codigo)
    ->get();
    return view('formularios.createVenta')->with('libro',$libro);

});

