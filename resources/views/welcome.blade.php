@include('fijos.header')

  <div class="wrapper">
    <div class="page-header clear-filter" filter-color="orange">
      <div class="page-header-image" data-parallax="true" style="background-image:url('./assets/img/mendoza-4267639_1920.jpg');">
      </div>
      <div class="container">
        <div class="content-center brand">
          <img style="height: 14rem; margin-bottom: 1rem;" src="./assets/img/LogoLibreriaBlanco.png" alt="">
          <!--h1 class="h1-seo">LIBRERIA EL PLATA</h1-->
          <h3>Libreria Independiente orientada a los Clásicos de la Literatura Universal.</h3>
          <div class="row">
            <div class="col-sm-6 mx-auto">
              <div class="row">
                <div class="col-sm-6">
                  <a href="catalogos/CatalogoElPlata.pdf" target="_blank" class="btn btn-outline-info btn-block bg-light" style="font-weight: 700;
                  color: darkcyan;">Catálogo</a>
                </div>
                <div class="col-sm-6">
                  <a href="/pedido/" style="font-weight: 700;
                  color: darkcyan;"class="btn btn-outline-info btn-block bg-light">Hacer Pedido</a>
                </div>
              </div>


            </div>
          </div>
        </div>
      </div>
    </div>

    <section id="quienes">
      <div class="container">
        <div class="section-story-overview">
          <div class="row">
            <div class="col-md-6">
              
              <div class="image-container image-left" style="background-image: url('assets/img/books-2596809_1920.jpg')">
                <!-- First image on the left side -->
                <!-- <p class="blockquote blockquote-primary">"Over the span of the satellite record, Arctic sea ice has been declining significantly, while sea ice in the Antarctichas increased very slightly"
                <br>
                <br>
                <small>-NOAA</small>
              </p> -->
              </div>
              <!-- Second image on the left side of the article -->
              <!--  <div class="image-container" style="background-image: url('../assets/img/bg3.jpg')"></div> -->
            </div>
            <div class="col-md-5">
              <!-- First image on the right side, above the article -->
              <div class="image-container image-right" style="background-image: url('assets/img/camping-4303357_1920.jpg')"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-10 mx-auto">
              <div class="row">
                <div class="col-sm-6">
                  <p>Estimado lector:</p>
                  <p>Desde hace algunos años, en la ciudad de Mendoza, un grupo de amigos entendemos la literatura como parte
                    importantísima en el enriquecimiento de la vida personal y por tal, de la restauración de la sociedad.
                    Ciertamente, pasó mucho tiempo hasta que pudimos resumir el asunto en la sentencia anterior. Obviamente,
                    tampoco pretendíamos haber descubierto nada nuevo, sobre las propiedades de los buenos libros mucho sabe
                    ya usted, y mucho más se ha escrito.
                  </p>
                  <p>Si bien es un hecho que cultivar el espíritu de un hombre lo trasciende colaborando con la mejoría de su
                    entorno; esta permanente aspiración espiritual también tiene incidencias en lo estrictamente material,
                    para bien y para mal. Sobre las bondades materiales no quiero insistir, porque los que nos conocen estarán
                    en desacuerdo y con razón. Pero el asunto es que notamos que nuestro tan común gusto por la literatura
                    empezaba a decantar y manifestarse en la formación de nuestras propias bibliotecas (o como nuestras
                    familias dirían: la acumulación de libros), y en el deseo incumplido de conseguir tal o cual obra; y más
                    frustrante aún, no poder pagarlas.
                  </p>
                </div>
                  <div class="col-sm-6">
                    <p>Una biblioteca llena de libros y polvo termina siendo el precio a pagar, y una tarea tan noble como es la
                      formación de una biblioteca personal, que en el futuro sería familiar, no puede llevarse adelante sin
                      paciencia. Eso nos movilizó a buscar y rebuscar la mejor forma de hacerlo. Desde entonces y paulatinamente
                      se fue formando lo que hoy es una realidad: una librería independiente. Nuestra afición por el Andinismo
                      sugirió nombrarla “El Plata”.
                    </p>
                    <p>Para nosotros el hecho de ser una librería independiente se manifiesta principalmente en el catálogo de
                      títulos que ofrecemos: es verdad que es limitado, pero justamente de eso de lo que nos jactamos; ya que es
                      el resultado de cuidadas valoraciones de las obras a difundir y desprendidos esfuerzos por conseguir
                      costes razonables. Es decir, prescindimos de libros de mala literatura o precios exorbitantes.
                    </p>
                    <p>Si su lectura llegó hasta aquí, le agradezco su atención y aprovecho para saludarlo muy cordialmente,</p>
                    
                   <p><strong>Pedro J.R.P.</strong></p> 
                     <i>Librería El Plata</i><br> 
                     <i> Mendoza - Argentina</i>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
 
  </section>
  <div class="main">
   
       <!-- SnapWidget -->
<iframe src="https://snapwidget.com/embed/914194" class="snapwidget-widget" allowtransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden;  width:1500px; height:300px"></iframe>
    <!-- <div class="section section-images">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="hero-images-container">
              <img src="assets/img/hero-image-1.png" alt="">
            </div>
            <div class="hero-images-container-1">
              <img src="assets/img/hero-image-2.png" alt="">
            </div>
            <div class="hero-images-container-2">
              <img src="assets/img/hero-image-3.png" alt="">
            </div>
          </div>
        </div>
      </div> -->
    </div>
  <div class="section" id="carousel" style="background: url('assets/img/adventure-2528477_1920.jpg')">
    <div class="container">
      
      <div class="row justify-content-center">
        <div class="col-lg-8 col-md-12">
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                <img class="d-block" src="assets/img/grimm.png" alt="First slide">
                <div class="carousel-caption d-none d-md-block">
                  
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block" src="assets/img/tierra de hombres.png" alt="Second slide">
                <div class="carousel-caption d-none d-md-block">
                  
                </div>
              </div>
              <div class="carousel-item">
                <img class="d-block" src="assets/img/amor.jpeg" alt="Third slide">
                <div class="carousel-caption d-none d-md-block">
                  
                </div>
              </div> 
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <i class="now-ui-icons arrows-1_minimal-left"></i>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <i class="now-ui-icons arrows-1_minimal-right"></i>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
@include('fijos.footer')