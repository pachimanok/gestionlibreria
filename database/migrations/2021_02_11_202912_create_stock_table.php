<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock', function (Blueprint $table) {
            $table->id();
            
            /* LIBRO */
            $table->bigInteger('codigo')->nullable();
            $table->string('titulo');
            $table->string('autor');
            $table->text('descripcion');
            $table->string('editorial');
            $table->string('tipo');
            $table->string('portada')->default('portada.png');
            $table->string('contra_tapa')->default('contra_tapa.png');

           /* Stock */
            $table->Integer('cantidad')->nullable();
            $table->decimal('precio_compra',8,2)->nullable();
            $table->decimal('precio_venta',8,2)->nullable();
            $table->decimal('precio_promocion',8,2)->nullable();
            $table->string('promocion')->nullable();
            $table->string('estado')->nullable();
            
            /* Prveedor */
            $table->string('proveedor')->nullable();
            $table->string('codigo_proveedor')->nullable();
            $table->integer('demora')->nullable();
            
            /* Publicidad */

            $table->string('recomendado')->nullable();
            $table->string('publicidad')->default('publicidad.png');
            $table->string('resena')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock');
    }
}
