@include('fijos.header')
<div class="wrapper">
    <div class="page-header clear-filter" filter-color="orange">
        <div class="page-header-image" data-parallax="true" style="background-image:url({{ asset('assets/img/mendoza-4267639_1920.jpg') }});">
        </div>
            <div class="container mt-5">
                <div class="card mt-5">
                    <div class="card-header">
                        <div class="card-title">
                            <h3>Hacer Pedido</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-10 mx-auto">
                                <div class="d-inline-flex p-2" style="width: inherit;">
                                    <div class="card" style="box-shadow: none !important;">
                                        <h2 class="text-center text-success mt-4 mb-1"><i class="now-ui-icons ui-1_zoom-bold"></i><hr style="margin: 2% 0% 2% 0%;    background: #5dedd9;"></h2>
                                        <p class="text-justify d-inline-flex text-center text-secondary">Buscar Código</p>
                                    </div>
                                    <hr>
                                    <div class="card" style="box-shadow: none !important;">
                                        <h2 class="text-center text-success mt-4 mb-1"><i class="now-ui-icons shopping_credit-card"></i><hr style="margin: 2% 0% 2% 0%;  background: #5dedd9;"></h2>
                                        <p class="text-justify d-inline-flex text-center text-secondary">Confirmar Compra</p>
                                    </div>
                                    <hr>
                                    <div class="card" style="box-shadow: none !important;">
                                        <h2 class="text-center text-success mt-4 mb-1"><i class="now-ui-icons shopping_delivery-fast"></i><hr style="margin: 2% 0% 2% 0%;    background: #5dedd9;"></h2>
                                        <p class="text-justify d-inline-flex text-center text-secondary">Mode de entrega</p>
                                    </div>
                                    <hr>
                                    <div class="card" style="box-shadow: none !important;">
                                        <h2 class="text-center text-success mt-4 mb-1"><i class="now-ui-icons education_agenda-bookmark"></i><hr style="margin: 2% 0% 2% 0%;     background: #5dedd9;"></h2>
                                        <p class="text-justify d-inline-flex text-center text-secondary">Recibir</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <form action="/buscar" method="POST">
                            @method('PUT')
                            @csrf
                        <div class="row">
                            <div class="col-sm-5 mx-auto">
                                <h3 class="text-center text-info mb-2">Colocar Código del Libro</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5 mx-auto">
                                <input type="text" name="codigo" class="form-control form-control-lg border-info" autofocus>
                        </div>
                    
                    </div>
                    <div class="row">
                        <div class="col-sm-8 mx-auto">
                        <p class="text-secondary">N° que aparece en el catálogo.</p>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 mx-auto">
                            <button type="submit" class="btn btn-info">Buscar</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
            <br>
        </div>
    </div>
</div>
@include('fijos.footer')