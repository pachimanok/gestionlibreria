<?php
//header("Location: mantenimiento.php");
//exit;


date_default_timezone_set('America/Buenos_Aires');
setlocale(LC_ALL,"es_AR.UTF8");
setlocale(LC_NUMERIC ,"en_US.UTF-8"); //punto en decimales


//DEBUG DE SESSIONES
//echo "gc_probability ".ini_get('session.gc_probability')."<br />"; //cuando es cero no ejecuta el gc collector y se ejecuta gc collector manualmente
//echo "gc_divisor ".ini_get('session.gc_divisor')."<br />";
//echo "gc_maxlifetime ".ini_get('session.gc_maxlifetime')."<br />";
//echo "cookie_lifetime ".ini_get('session.session.cookie_lifetime')."<br />";
//echo "save_path ".ini_get('session.session.save_path')."<br />";

//Sessiones
//Probabilidad 1 en 1000
//ini_set('session.gc_probability',1);
//ini_set('session.gc_divisor',1000);

// FECHAS LIMITE DE POLIZA
// Entre 14 y 80 Años
// tienen cobertura de 14 a 65 años. 
// Y para los mayores a 65 años y hasta 80 años tendrán cobertura pero con un 50% 
// de suma asegurada. INCLUSO ESTO ESTA DESCRIPTO EN LOS CERTIFICADOS DE MERIDIONAL Y MERCANTIL



//El cache de codeigniter usa la funcion preg_match que tiene por default un limite de 1 mega
//echo ini_get('pcre.backtrack_limit');
//exit;
//la aumento a 5 veces el default es 1000000
ini_set('pcre.backtrack_limit', '5000000');


define('ASEGURADO_FECHA_MINIMA',(date("Y", time())-80).'-'.date('m').'-'.date('d'));
define('ASEGURADO_FECHA_MAXIMA',(date("Y", time())-14).'-'.date('m').'-'.date('d'));

//PARA CONFIGURAR EL DATEPICKER JQUERY
define('ASEGURADO_ANIO_MINIMA',(date("Y", time())-80));
define('ASEGURADO_ANIO_MAXIMA',(date("Y", time())-14));







if($_SERVER['SERVER_NAME']=="localhost") {
    $path=  "/web/paguiar/taker_ap_2017/";
    $url=   "http://localhost/paguiar/taker_ap_2019/";
    
    //URLS
    define('BASEURL',          $url);
    define('FRONTURL',         $url);
    define('URI_PROTOCOL',      "AUTO");
    define('FOTOURL',          $url.'foto/');
    define('DOCUMENTOSURL',    $url.'documentos/');
    define('FACTURASSURL',     'http://localhost/t/facturas/');  
    define('BANNERSURL',       $url.'banners/');
    define('CAPTCHAURL',       $url.'captcha/');


    //PATHS
    define('UPLOADPATH',       $path.'upload/');    //Lo usa el upload de fotos
    define('FOTOPATH',         $path.'foto/');      //luego lo mueve a esta carpeta
    define('CAPTCHADPATH',     $path."captcha/");
    define('DOMPDFPATH',       '/web/paguiar/taker_dompdf/');
    define('PDF_IMAGES',       $path."imgs/");  //los logos de las aseguradoras
    define('DOCUMENTOSPATH',   $path.'documentos/');
    define('FACTURASPATH',     '/web/t/');
    define('BARCODEPATH',       $path.'barcode/');
    define('IMAGEBARCODEPATH',  $path.'img_barcode/');     
    

    //BASE DE DATOS
    //LUEGO DE MIGRAR PONER PCONNECT EN TRUE !!!
    define('DATABASE_HOST',    'localhost');
    define('DATABASE',         'taker_ap_2019');
    define('DATABASE_USER',    'root');
    define('DATABASE_PASS',    'gandalf9');
    define('DATABASE_DEBUG',   true);
     
    //CORREO
    define('CORREO_ACTIVO',true); 
    
    //testing: User=62042&Password=154598
    //produccion: User=472590&Password=418318
    define('NOSIS_URL','http://localhost/paguiar/taker_ap_2017/nosis_consulta.php?IdConsulta={id_consulta}&TipoConsulta=1&Doc={documento_numero}&Resp_PorDocExacto=Si&Resp_DomTipoInfo=3&User=472590&Password=418318');
    //define('NOSIS_URL','http://sac.nosis.com.ar/Sac_ServicioVI/Consulta.asp?IdConsulta={id_consulta}&TipoConsulta=1&Doc={documento_numero}&Resp_PorDocExacto=Si&Resp_DomTipoInfo=3&User=62042&Password=154598');
    //CODEIGNITER
    $system_path =          $path."system";
    $application_folder =   $path.'application';
    $view_folder =          $path.'application/views';
    
    define('ENVIRONMENT',   'development');
    
    //ERRORES
    error_reporting(E_ALL);
    ini_set('display_errors', true);
    
}else{
    //PRODUCCION
    $path=  "/var/www/www.taker.com.ar/html/taker_ap_barrios/";
    $url=   "https://www.taker.com.ar/taker_ap_barrios/";
    //$url=   "/taker_ap_barrios/";
    
     //URLS
    define('BASEURL',          $url);
    define('FRONTURL',         $url);
    define('URI_PROTOCOL',      "AUTO");
    define('FOTOURL',          $url.'foto/');
    define('DOCUMENTOSURL',    $url.'documentos/');
    //TODO: Luego de migrar usar la url de facturas absoluta
    define('FACTURASSURL',     '/t/');  
    define('BANNERSURL',       $url.'banners/');
    define('CAPTCHAURL',       $url.'captcha/');
    //PATHS
    define('UPLOADPATH',       $path.'upload/');    //Lo usa el upload de fotos
    define('FOTOPATH',         $path.'foto/');      //Luego mueve la foto
    define('CAPTCHADPATH',     $path."captcha/");
    define('DOMPDFPATH',       '/var/www/www.taker.com.ar/html/taker_dompdf/');
    define('PDF_IMAGES',       $path.'imgs/');  //los logos de las aseguradoras
    
    define('DOCUMENTOSPATH',   $path.'documentos/');
    define('FACTURASPATH',     '/var/www/www.taker.com.ar/html/t/');
    
    define('BARCODEPATH',       $path.'barcode/');
    define('IMAGEBARCODEPATH',  $path.'img_barcode/');    
    //BASE DE DATOS
    //LUEGO DE MIGRAR PONER PCONNECT EN TRUE !!!
    define('DATABASE_HOST',    'localhost');
    define('DATABASE',         'taker_ap_2021');
    define('DATABASE_USER',    'taker_user');
    define('DATABASE_PASS',    '987h20ufa0s8nq08hew');
    define('DATABASE_DEBUG',   FALSE);
     
    //CORREO
    define('CORREO_ACTIVO',true);  
     
    //NOSIS
    //http://sac.nosis.com.ar/Sac_ServicioVI/Consulta.asp
    //testing: User=62042&Password=154598
    //produccion: User=472590&Password=418318
    define('NOSIS_URL','http://sac.nosis.com.ar/Sac_ServicioVI/Consulta.asp?IdConsulta={id_consulta}&TipoConsulta=1&Doc={documento_numero}&Resp_PorDocExacto=Si&Resp_DomTipoInfo=3&User=472590&Password=418318');
     
     
    //CODEIGNITER
    $system_path =          $path."system";
    $application_folder =   $path.'application';
    $view_folder =          $path.'application/views';
    
    define('ENVIRONMENT',   'production');


}

//USUARIO ID DE TAKER, beneficiario de los asegurados de los barrios
define("TAKER_ID",27626);

//ACTIVIDAD DEFAULT DE TAKER, Producto 4 - CONSTRUCCION HASTA 15 MTS. DE ALTURA
define("TAKER_ACTIVIDAD_ID",535);

//ID DEL USUARIO EMPRESA DEMO, ES UNA EMPRESA, 
//no factura solo el usuario ve los ingresos
define("EMPRESA_DEMO_ID",34648); 

define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
define('BASEPATH', $system_path.DIRECTORY_SEPARATOR);
define('FCPATH', dirname(__FILE__).DIRECTORY_SEPARATOR);
define('SYSDIR', basename(BASEPATH));
define('APPPATH', $application_folder.DIRECTORY_SEPARATOR);
define('VIEWPATH', $view_folder.DIRECTORY_SEPARATOR);

require_once BASEPATH.'core/CodeIgniter.php';
