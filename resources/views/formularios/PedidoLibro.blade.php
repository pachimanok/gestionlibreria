 
@include('fijos.header')
@foreach ($libro as $libro)
<div class="wrapper">
    <div class="page-header clear-filter" filter-color="orange">
        <div class="page-header-image" data-parallax="true" style="background-image:url({{ asset('assets/img/mendoza-4267639_1920.jpg') }});">
        </div>
        <div class="container mb-5">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        <h3 style="color:black;" class="text-info mb-0 mt-1"> Resultado: {{$libro->codigo}}</h3>
                        <p class="text-success">{{$libro->estado}}</p>
                    </div>
                </div>
                <div class="card-body">
                    
                    <div class="row">
                        <div class="col-sm-1"></div>
                        <div class="col-sm-3">
                            <img src="{{ asset('portadas/'.$libro->portada) }}" alt="">
                        </div>
                        <div class="col-sm-7">
                            <p style="color: gray; margin:0;"><strong>Autor:</strong> {{$libro->autor}}</p>
                            <p style="color: gray; margin:0;"><strong>Editorial:</strong> {{$libro->editorial}}</p>
                            <p style="color: gray; margin:0;"><strong>Titulo:</strong> {{$libro->titulo}}</p>
                            <p style="color: gray; margin:0;"><strong>Descripcion:</strong></p>
                            <p style="color: gray; margin:0;">{{$libro->descripcion}}</p>
                            <h3 class="text-center text-info mb-2 mt-2" style="font-weight:800;">${{$libro->precio_venta}}</h3>     
                            <br>
                            <form action="/venta/create/{{$libro->codigo}}" method="post">
                                @method('GET')
                                @csrf
                                <div class="row">
                                    <div class="col-sm-5 mx-auto">
                                        <button type="submit"  class="btn btn-info mt-2 btn-block">Comprar</button>
                                    </div>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        @endforeach
<footer id="contacto" class="footer" data-background-color="black">
    <div class="container">
        <div class="col-sm-7 mx-auto text-center">
            <div class="social-line">
            <a href="https://t.me/joinchat/UhWDvMUSXZgHGJMc"  target="_blank" class="btn btn-neutral btn-facebook btn-icon btn-round">
                <i class="fab fa-telegram"></i>
            </a>
            <a href="https://api.whatsapp.com/send?phone=542613873787&text=Hola, me comunico desde la página!" target="_blank"class="btn btn-neutral btn-facebook btn-icon btn-lg btn-round">
                <i class="fab fa-whatsapp"></i>
            </a>
            <a href="https://www.instagram.com/elplatalibreria/" class="btn btn-neutral btn-google btn-icon btn-round">
                <i class="fab fa-instagram"></i>
            </a>
            </div>
        
            &copy;Libreria el Plata
            <script>
                document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>, Designed by
            <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> & 
            <a href="http://builditdesing.com" target="_blank">Build IT 4.0</a> 
         </div>
    </div>
</footer>
  
  <!--   Core JS Files   -->
  <script src="./assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="./assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="./assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="./assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="./assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="./assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
  <script>
    $(document).ready(function () {
      // the body of this function is in assets/js/now-ui-kit.js
      nowuiKit.initSliders();
    });

    function scrollToDownload() {

      if ($('.section-download').length != 0) {
        $("html, body").animate({
          scrollTop: $('.section-download').offset().top
        }, 1000);
      }
    }
  </script>
</body>

</html>
