<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVentasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ventas', function (Blueprint $table) {
            $table->id();
            
            /* LIBRO */
            $table->bigInteger('codigo')->nullable();
            
           /* Comercial */
            $table->string('vendedor')->nullable();
            $table->Integer('cantidad')->nullable();
            $table->decimal('precio_total',8,2)->nullable();
            $table->decimal('costo_total',8,2)->nullable();
            $table->decimal('precio_venta',8,2)->nullable();
            $table->decimal('costo_venta',8,2)->nullable();
            $table->decimal('comision',8,4)->nullable();
            $table->Integer('puntos')->nullable();
            
            /* Cliente */
            $table->string('tipo')->nullable();
            $table->string('nombre_pedido')->nullable();
            $table->bigInteger('celular_pedido')->nullable();
            $table->string('nombre_entrega')->nullable();
            $table->bigInteger('celular_entrega')->nullable();
            $table->text('dedicatoria')->nullable();
            $table->string('direccion_entrega')->nullable();
            $table->string('localidad')->nullable();
            $table->string('provincia')->nullable();
            $table->string('horario')->nullable();

            /* Estado */

            $table->string('estado')->nullable();
            $table->string('estado_pago')->nullable();
            $table->string('cuotas')->nullable();
            $table->string('cuotas_cantidad')->nullable();

            /* Satisfaccion */

            $table->integer('calificacion_atencion')->nullable();
            $table->integer('calificacion_entrega')->nullable();
            $table->integer('calificacion_libro')->nullable();
            $table->integer('calificacion_libreria')->nullable();
            $table->integer('calificacion_comercial')->nullable();
            $table->text('comentario_general')->nullable();
            $table->text('comentario_libro')->nullable();
            $table->string('suscripcion')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
