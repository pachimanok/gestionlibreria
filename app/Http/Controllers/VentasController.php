<?php

namespace App\Http\Controllers;

use App\Models\Venta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class VentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'Hola Chavon'/* view('formularios.createVenta') */;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* Nos traemos el código del Libro */
        
        $codigo = $request->get('codigo');

        /* Revisamos la Cantidad que tiene y actualizamos en el Stock */

        $c = DB::table('stock')->select('cantidad','precio_compra')->where('codigo', $codigo)->get();
        $c = $c[0];
        $ca = $c->cantidad;
        $cantidad = $ca - 1 ; 

        $precio_compra = $c->precio_compra;
        
        DB::table('stock')->where('codigo', $codigo)
        ->update(['cantidad' => $cantidad]);

        /* Revisamos si hay que cobrar precio promosión o Precio de Venta */

        $promocion = DB::table('stock')->select('promocion')->where('codigo',"=",$codigo)->get();
        
        if($promocion == 'no'){

            $precio_venta = DB::table('stock')->select('precio_venta')->where('codigo',"=",$codigo)->get();
            $precio_venta = $precio_venta[0];
            $precio_venta = $precio_venta->precio_promocion;

        }else{

            $precio_venta = DB::table('stock')->select('precio_promocion')->where('codigo',"=",$codigo)->get();
            $precio_venta = $precio_venta[0];
            $precio_venta = $precio_venta->precio_promocion;

        }

        /* Revisamos que tipo de envio tiene */

        $tipo = $request->get('tipo');

        /* Si es con retito concretamos la Venta */
        
        if($tipo == 'retiro') {
            foreach ($request->get('modo_pago') as $modo_pago);
            
            $ventas = new Venta();

            $ventas->codigo = $codigo;
            $ventas->nombre_pedido = $request->get('nombre_pedido');
            $ventas->celular_pedido = $request->get('celular_pedido');
            $ventas->nombre_entrega = $request->get('nombre_entrega');
            $ventas->celular_entrega = $request->get('celular_entrega');
            $ventas->dedicatoria = $request->get('dedicatoria');
            $ventas->tipo = $tipo;
            $ventas->precio_venta = $precio_venta;
            $ventas->costo_venta = $precio_compra;
            $ventas->precio_total= $precio_venta;
            $ventas->costo_total= $precio_compra;
            $ventas->estado= 'en preparación';
            $ventas->estado_pago= 'no pagado';
            $ventas->modo_pago = $modo_pago;


            $ventas->save();

            $ultimo = Venta::select('id')->orderBy('id', 'DESC')->first();
            $ultimo = $ultimo->id;
            

            $compra = DB::table('stock')
            ->select('stock.titulo','stock.autor','stock.editorial','stock.editorial','ventas.*')
            ->join('ventas', 'ventas.codigo','=','stock.codigo')
            ->where('ventas.id','=',$ultimo)
            ->get();

            return view('formularios.VentaConcretada')->with('compra', $compra);

        }else{

            $ventas = new Venta();
        
            $ventas->nombre_pedido = $request->get('nombre_pedido');
            $ventas->codigo = $codigo;
            $ventas->celular_pedido = $request->get('celular_pedido');
            $ventas->nombre_entrega = $request->get('nombre_entrega');
            $ventas->celular_entrega = $request->get('celular_entrega');
            $ventas->dedicatoria = $request->get('dedicatoria');
            $ventas->tipo = $tipo;
            $ventas->precio_venta = $precio_venta;

            $ventas->save();

            $ultimo = Venta::select('id')->first();
            $ultimo = $ultimo->id;

            $compra = DB::table('stock')
            ->select('stock.titulo','stock.autor','stock.editorial','stock.editorial','ventas.*')
            ->join('ventas', 'ventas.codigo','=','stock.codigo')
            ->where('ventas.id','=',$ultimo)
            ->get();
            return view('formularios.VentaEnvio')->with('compra', $compra);
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
