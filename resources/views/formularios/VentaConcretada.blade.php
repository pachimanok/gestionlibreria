 
@include('fijos.header')

<div class="wrapper">
    <div class="page-header clear-filter" filter-color="orange">
        <div class="page-header-image" data-parallax="true" style="background-image:url({{ asset('assets/img/mendoza-4267639_1920.jpg') }});">
        </div>
     @foreach ($compra as $compra)
        <div class="container mt-5 mb-5 p- 5">
            <div class="col-sm-10 mx-auto">
                <div class="card"> 
                    <div class="card-body mt-3">
                        <h3 class="text-info text-center">¡Felicitaciones has comprado <strong>{{ $compra->titulo }}</strong></h3>
                        <h4 class="text-info text-center">ID:000{{$compra->id }}</h4>
                        <p class="text-secondary text-center"> Podrás retirarlo por nuestros puntos de ventas: <br>
                        Roque Saenz Peña 5821. Guaymallen, Mendoza. <br>
                        Beltrán 1812 . Godoy Cruz, Mendoza <br> 
                        Al momento de retirar debera abonar:
                        </p>
                        <h2 class="text-info text-center">${{ $compra->precio_total}}</h2>
                        <br>
                        <a href="https://api.whatsapp.com/send?phone=542613873787&text=Hola,%20quiero%20coordinar%20mi%20pedido!%20ID:000{{$compra->id }}" class="bnt btn-outline-info">Coordinar retiro</a>
                        <br>
                        <p class="text-center text-secondary mt-2"> <small> tecnologia: build.it</small></p>
                    </div> 
                </div>
            </div>
        </div>
    @endforeach
</div>
<footer id="contacto" class="footer" data-background-color="black">
    <div class="container">
        <div class="col-sm-7 mx-auto text-center">
            <div class="social-line">
            <a href="https://t.me/joinchat/UhWDvMUSXZgHGJMc"  target="_blank" class="btn btn-neutral btn-facebook btn-icon btn-round">
                <i class="fab fa-telegram"></i>
            </a>
            <a href="https://api.whatsapp.com/send?phone=542613873787&text=Hola, me comunico desde la página!" target="_blank"class="btn btn-neutral btn-facebook btn-icon btn-lg btn-round">
                <i class="fab fa-whatsapp"></i>
            </a>
            <a href="https://www.instagram.com/elplatalibreria/" class="btn btn-neutral btn-google btn-icon btn-round">
                <i class="fab fa-instagram"></i>
            </a>
            </div>
        
            &copy;Libreria el Plata
            <script>
                document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))
            </script>, Designed by
            <a href="https://www.invisionapp.com" target="_blank">Invision</a>. Coded by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> & 
            <a href="http://builditdesing.com" target="_blank">Build IT 4.0</a> 
         </div>
    </div>
</footer>
  
  <!--   Core JS Files   -->
  <script src="./assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="./assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="./assets/js/core/bootstrap.min.js" type="text/javascript"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="./assets/js/plugins/bootstrap-switch.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="./assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
  <script src="./assets/js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
  <script src="./assets/js/now-ui-kit.js?v=1.3.0" type="text/javascript"></script>
  <script>
    $(document).ready(function () {
      // the body of this function is in assets/js/now-ui-kit.js
      nowuiKit.initSliders();
    });

    function scrollToDownload() {

      if ($('.section-download').length != 0) {
        $("html, body").animate({
          scrollTop: $('.section-download').offset().top
        }, 1000);
      }
    }
  </script>
</body>

</html>
